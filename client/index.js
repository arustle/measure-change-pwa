/* global module: true */
import React from 'react';
import ReactDOM from 'react-dom';
// import './index.scss';
import App from './App';
import VAPID from './../private/vapid.json';

// import 'file-loader?name=./web-app-manifest.json!./web-app-manifest.json';

ReactDOM.render((<App />), document.getElementById('root'));

if (module.hot) {
  module.hot.accept(function () {
    console.log('Accepting the updated printMe module!');
  });
}