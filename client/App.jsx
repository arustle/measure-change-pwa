import React, { Component } from 'react';
import {
  BrowserRouter as Router,
  Route
} from 'react-router-dom';


class App extends Component {
  constructor(props) {
    super(props);
    
  }

  render() {
    let wrapperClassNames = ['frontend-grocer'];
    
    return (
      <Router>
        <div className={wrapperClassNames.join(' ')}>
        Test
        </div>
      </Router>
    );
  }
}

export default App;